@extends("la.layouts.app")
@section("contentheader_title")

<?php
// start the session
session_start();
// form token 
$csrf_token = uniqid();

// create form token session variable and store generated id in it.
$_SESSION['csrf_token'] = $csrf_token;
?>
Apply For Reimbursement
@endsection

@section("main-content")
@if(count($errors))
<div class="form-group">
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif
<div class="box entry-form">
    <div class="box-body">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form method="POST" action="{{url(config('laraadmin.adminRoute').'/reimbursement_forms/store')}}" enctype="multipart/form-data" data-same-app="<?php echo session('employee_details')->first_approver == session('employee_details')->second_approver; ?>" id="add_form">
                    <input type="hidden" name="_token" value="{{ csrf_token()}}">
                    <input type="hidden" id="verified_approval" name="verified_approval" value="1">
                    <input type="hidden" id="hard_copy_attached" name="hard_copy_attached" value="1">
					

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label>Manager Name</label>
                            <input type="text" class="form-control" value="{{$manager}}" disabled/>
                        </div>
                        <div class="form-group col-md-3 hide">
                            <label for="Name">Employee Id:</label>
                            <input type="text" class="form-control" name="emp_id" autocomplete="off" value="<?php echo Auth::user()->context_id; ?>" id="emp_id" placeholder="EmpId" required readonly>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="Date" class="control-label">Date*</label>
                            <input class="form-control readonly" id="date" name="date"  type="text"  autocomplete="off" required="true"/>
                        </div>

                        <div class="form-group col-md-4">
                            <label>Reimbursement Type</label>
                            <select name="type_id" id="type_id" class="form-control" >
							<?php
                                  if (!empty($reimbursement_types)) {
                                        foreach ($reimbursement_types as $value) {
                                              echo '<option data-doc-req="' . $value->document_required . '" value="' . $value->id . '" data-limit="' . $value->limit . '" data-limit_variance="' . $value->limit_variance . '" data-hard-copy="' . $value->document_required . '" data-verification-level="' . $value->verification_level . '">' . $value->name . ' </option>';
                                        }
                                  }
                                  ?>
                                 
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="Cosharing" class="control-label">Co-Sharing Names</label>
                            <select name="cosharing[]" id="cosharing" class="js-example-basic-multiple" multiple="multiple"   >
                                  <?php
                                  if (!empty($employeename)) {
                                        foreach ($employeename as $value) {
                                              echo '<option value="' . $value->id . '">' . $value->name . '</option>';
                                        }
                                  }
                                  ?> 
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="Cosharing_count"  class="control-label">Emp Count</label>
                            <input type="text" value="{{ old('Cosharing_count')}}" class="form-control" 
                                   id="cosharingcount"  name="Cosharing_count" autocomplete="off" readonly="true" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="Amount" class="control-label">Amount (INR)*</label>
                            <input type="text" value="{{ old('amount')}}" class="form-control" 
                                   id="amount"  name="amount" autocomplete="off"  placeholder="Amount" maxlength="9" required="true"/>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="Number">User Comment*</label>
                            <input type="text" value="{{ old('user_comment')}}" class="form-control" name="user_comment" autocomplete="off"   maxlength="180" id="user_comment" required="true" >   
                        </div>


                        <div class="form-group col-md-3" >

                            <label for="Number">Bill Required:</label>

                            <table>
                                <tr>
                                    <td id="doc-att-yes">
                                        <label style="margin-right: 20px;"><input type="radio"  name="document_attached" value="1" class="check" checked required  > Yes</label>
                                    </td>
                                    <td id="doc-att-no">
                                        <label><input type="radio"  name="document_attached" value="0" checked class="uncheck"  > No</label>
                                    </td>
                                </tr>
                            </table>

                        </div>

                        <div class="form-group col-md-9 imageupload" style="display: none" id="document_attached1">

                            <label for="Number"> Select image :</label>
                            <input  type="file" id="name" class="form-control" name="name[]" placeholder="address" multiple onchange="validateImage()" value="{{ old('name')}}" >

                        </div>

                        <div class="col-md-12 text-right" >
                            <button type="submit" onclick="CheckApproval()" class="btn btn-success">Submit</button>
							 <input class="btn btn-danger"  action="action" onclick="window.history.go(-1); return false;" type="button" value="Cancel" />
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
                                  var limit = 0;
                                  var limit_variance = 0;
                                  //        var level=0;

                                  $(".js-example-basic-multiple-limit").select2(
                                          {
                                                maximumSelectionLength: 25

                                          });
										  
										   $(".readonly").keydown(function(e){
                                               e.preventDefault();
                                               });
                                  $(document).ready(function () {
									  								  
									  $("#cosharing").change(function () {
											var count = ($(this).val() != null)? $(this).val().length : 0;
                                            $("#cosharingcount").val(count);
                                        });
										
                                        var emp_detail = "{{ Session::get('employee_details') }}";
                                        emp_detail = JSON.parse(emp_detail.replace(/&quot;/g, '\"'));
                                        //get dates from session
                                        var dates = "{{ Session::get('holiday_list') }}";
                                        dates = JSON.parse(dates.replace(/&quot;/g, '\"'));

                                        $('select').select2();

                                        //Show/hide comp off list
//                                    $('#type_id').on('change', function () {
//                                        ReimbursementType(this);
//                                    });

                                      
                                    $("#date").change(function () {
                                        if ($(this).val() != '') {
                                            $(this).siblings("label#date-error").hide();
                                            $(this).removeClass("error");
                                        }
                                    });



                                        $("input[name$='document_attached']").click(function () {
                                              var test = $(this).val();
                                              $('.uncheck').click(function () {
                                                    $("div.imageupload").hide();
                                                    ($("#name").val(''));
                                              });
                                              if (test == 1) {
                                                    $("#document_attached1").show();
                                                    $("#name").attr('required', true).show();

                                              } else {
                                                    $("div.imageupload").hide();
                                                    $("#name").attr('required', false).hide();
                                              }
                                        });
                                        $("#amount, #cosharingcount").keypress(function (e) {


                                              if (this.value.length == 0 && e.which == 48) {
                                                    return false;
                                              }

                                              if (e.which == 46) {
                                                    if ($(this).val().indexOf('.') != -1) {
                                                          return false;
                                                    }
                                              }

                                              if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
                                                    return false;
                                              }
                                        });


                                        $("#type_id").change(function () {

                                              var req = $(this).children("option:selected").attr("data-doc-req");
                                              var hard = $(this).children("option:selected").attr("data-hard-copy");
                                              //   level = $(this).children("option:selected").attr("data-verification-level");
                                              limit_variance = $(this).children("option:selected").attr("data-limit_variance");
                                              limit = $(this).children("option:selected").attr("data-limit");


                                              if (hard == 'Yes') {
                                                    $('#doc-att-yes').find('input').prop("checked", "checked").trigger('click');
                                                    $('#doc-att-no').find('input').prop("checked", false);
                                                    $('#doc-att-no').hide();

                                              } else {
                                                    $('#doc-att-yes').find('input').prop("checked", false);
                                                    $('#doc-att-no').find('input').prop("checked", "checked").trigger('click');
                                                    $('#doc-att-no').show();
                                              }
                                              $("#hard_copy_attached").val(hard);

                                        })
                                        $("#type_id").trigger("change");

									  $("span.select2-selection__rendered").removeAttr("title");
									  $('#type_id').change(function(){
										$("span.select2-selection__rendered").removeAttr("title");
									  });
                                                        $("span.select2-selection__rendered").removeAttr("title");
									  $('#cosharing').change(function(){
										$("li.select2-selection__choice").removeAttr("title");
									  });   

                                  });

                                  function CheckApproval() {
                                        var employee = $("#cosharingcount").val() != '' ? parseInt($("#cosharingcount").val()) : 0;
                                        var emptotal = parseInt(employee) + 1;
                                        var actualAmout = parseFloat($("#amount").val());
                                        var amount = emptotal * (limit * limit_variance / 100);
                                        var realamount = parseFloat(limit * emptotal);
                                        var total = parseFloat(realamount) + parseFloat(amount);
                                        var same_approver = $("#add_form").attr('data-same-app');
                                        if (actualAmout < total || same_approver) {
                                              $("#verified_approval").val(2);
                                        } else {
                                              $("#verified_approval").val(3);
                                        }

                                  }

                                   var d = new Date();
                                  var CurrentYear = (d.getMonth() < 3) ? d.getFullYear()-1 : d.getFullYear() ;
                                  $(function () {
                                       $("#date").datepicker({
                                              dateFormat: 'dd M yy',
                                              todayHighlight: 'true',
                                              changeMonth: true,
                                              changeYear: true,
                                              minDate: new Date((CurrentYear),3, 01),
                                              maxDate: '+0day',
                                              numberOfMonths: 1

                                        });

                                  });



                                  function validateImage() {
                                        var img = $("#name").val();

                                        var exts = ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'pdf'];
                                        // split file name at dot
                                        var get_ext = img.split('.');
                                        // reverse name to check extension
                                        get_ext = get_ext.reverse();

                                        if (img.length > 0) {
                                              if ($.inArray(get_ext[0].toLowerCase(), exts) > -1) {
                                                    return true;
                                              } else {
                                                    swal({
                                                          title: "Error",
                                                          text: "Upload only jpg, jpeg, png, gif, pdf, bmp images",
                                                          type: "error"
                                                    }).then(() => {
                                                          $('#name').val('');
                                                    });

                                                    return false;
                                              }
                                        } else {
                                              swal("please upload an image");
                                              return false;
                                        }

                                        return false;
                                  }
								  
								  $('#amount').bind("cut copy paste drop drag",function(e) {
     e.preventDefault();
 });
 var txt = document.getElementById('amount');
txt.addEventListener('keyup', myFunc);

function myFunc(e) {
    var val = this.value;
    var re = /^([0-9]+[\.]?[0-9]?[0-9]?|[0-9]+)$/g;
    var re1 = /^([0-9]+[\.]?[0-9]?[0-9]?|[0-9]+)/g;
    if (re.test(val)) {
        //do something here

    } else {
        val = re1.exec(val);
        if (val) {
            this.value = val[0];
        } else {
            this.value = "";
        }
    }
}



</script>

@endpush

